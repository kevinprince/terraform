# Terraform Modules

These are my core terraform modules for setting applications in AWS.

## Whats included:

- VPC
- Auto Scaling Group
- Load Balancer (ALB only)

## Testing

All modules are tested with InSpec and output can be seen in the CI flow.